### Import relevant packages
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.setrecursionlimit(10000)
from sklearn.preprocessing import MinMaxScaler, StandardScaler
scaler = StandardScaler()

### Read in the necessary datapoints
full_df = pd.read_pickle('datasets/full_dataframe.pkl')
tagged_phish_df = pd.read_pickle('datasets/tagged_phish_df.pkl')

### Functions for Post-Clustering Evaluation
#Internal Evaluation metrics
def internal_eval(emb_vect, labels):
    from sklearn.metrics import davies_bouldin_score, silhouette_score
    sls = silhouette_score(emb_vect,labels)
    dbi = 1/davies_bouldin_score(emb_vect,labels)
    return [sls, dbi]

#External Evaluation metrics
def external_eval(labels_pred):
    from sklearn.metrics import adjusted_rand_score, adjusted_mutual_info_score, v_measure_score
    labels_true = tagged_phish_df.drop(tagged_phish_df[tagged_phish_df.content_tag == 0].index).content_tag
    labels_pred = list(labels_pred[labels_true.index])
    e1 = adjusted_rand_score(labels_true, labels_pred)
    e2 = adjusted_mutual_info_score(labels_true, labels_pred)
    e3 = v_measure_score(labels_true, labels_pred)
    return [e1, e2, e3]

#Phish/Benign Homogeneity - probability of major class in each cluster
def homogeneity_eval(labels):
    df = pd.DataFrame()
    df['cluster_label'] = labels
    df['tag'] = list(full_df.Tag)
    major_class = []
    for i in set(df.cluster_label):
        temp = df[df.cluster_label == i]
        n_phish = temp[temp['tag'] == 'Phish'].shape[0]
        n_benign = temp[temp['tag'] == 'Benign'].shape[0]
        if n_phish >= n_benign:
            p2 = (n_phish/(n_phish + n_benign))
        else:
            p2 = (n_benign/(n_phish + n_benign))
        major_class.append(p2)
    score = sum(major_class)/len(set(df.cluster_label))
    return [score]

#Cluster-wise analysis of homogeneity
def cluster_analysis(df, labels, full_df):
    df['clusters'] = labels
    for i in list(set(labels)):
        t = list(df[df['clusters'] == i].index)
        temp = full_df.iloc[t]
        n_phish = temp[temp['Tag'] == 'Phish'].shape[0]
        n_benign = temp[temp['Tag'] == 'Benign'].shape[0]
        print("For cluster", i, temp.shape[0],": ")
        print("The percentage of phishing emails are: ", 100*(n_phish/(n_phish + n_benign)))
        print("The percentage of benign emails are: ", 100*(n_benign/(n_phish + n_benign)))

#Distribution of campaign tags among the clusters
def tag_analysis(tag, labels):
    tagged_phish_df = pd.read_pickle('datasets/tagged_phish_df.pkl')
    df['clusters'] = labels
    import collections
    temp_index = list(tagged_phish_df[tagged_phish_df.content_tag == tag].index)
    temp_df = df.iloc[temp_index]
    cluster_dist = collections.Counter(list(temp_df.clusters))
    for u in cluster_dist.items():
        print(u[1], 'emails are in cluster', u[0])
    print()

# Key-based tags to analyse the distribution
key_dict = pickle.load(open('datasets/keys_dict.pkl', 'rb'))
tags = [i+1 for i in range(len(key_dict))]
keys = list(key_dict.keys())


# Read the input vectors for clustering
feat_rep_e = pd.DataFrame(pickle.load(open('outputs/feat_rep_e.pkl', 'rb')))
feat_rep_s = pd.DataFrame(pickle.load(open('outputs/feat_rep_s.pkl','rb')))
feat_rep_u = pd.read_pickle('outputs/feat_rep_u.pkl')
feat_rep_h = pd.read_pickle('outputs/feat_rep_h.pkl')
print('input data read ...')


vect1 = pd.DataFrame(scaler.fit_transform(feat_rep_e.to_numpy()))
vect2 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_h], ignore_index=True, axis=1).to_numpy()))
vect3 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_u], ignore_index=True, axis=1).to_numpy()))
vect4 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_s], ignore_index=True, axis=1).to_numpy()))
vect5 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_s, feat_rep_u], ignore_index=True, axis=1).to_numpy()))
vect6 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_s, feat_rep_h], ignore_index=True, axis=1).to_numpy()))
vect7 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_e, feat_rep_s, feat_rep_u, feat_rep_h], ignore_index=True, axis=1).to_numpy()))
base_line_1 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_u, feat_rep_h], ignore_index=True, axis=1).to_numpy()))
base_line_2 = pd.DataFrame(scaler.fit_transform(pd.concat([feat_rep_s, feat_rep_u, feat_rep_h], ignore_index=True, axis=1).to_numpy()))
print('input vectors defined ...')

print()
print('Vector 1: email body text')
print('Vector 2: email body text + header')
print('Vector 3: email body text + URL')
print('Vector 4: email body text + subject')
print('Vector 5: email body text + subject + URL')
print('Vector 6: email body text + subject + header')
print('Vector 7: email body text + subject + URL + header')
print()


x = int(input('Enter input vector number:' ))

if x == 1:
    input_vect = vect1
elif x == 2:
    input_vect = vect2
elif x == 3:
    input_vect = vect3
elif x == 4:
    input_vect = vect4
elif x == 5:
    input_vect = vect5
elif x == 6:
    input_vect = vect6
elif x == 7:
    input_vect = vect7


### Function to run clustering across a range of parameters
def agg_clustering(n):
    from sklearn.cluster import AgglomerativeClustering
    ag_model = AgglomerativeClustering(n_clusters=n).fit(input_vect)
    labels = ag_model.labels_
    eval_metrics = external_eval(labels) + homogeneity_eval(labels) + internal_eval(input_vect, labels) 
    return eval_metrics


### Run clustering over a range for coarse grained parameter sweep
print('Performing coarsed-grained parameter sweep ...')
eval_scores = []
for i in range(30,100,3):
    temp = agg_clustering(i)
    temp.append(i)
    eval_scores.append(temp)


### Plotting the n vs accuracy
import matplotlib.pyplot as plt

x = [item[6] for item in eval_scores]  #number of clusters
y1 = [item[0] for item in eval_scores] #ari
y2 = [item[1] for item in eval_scores] #ami
y3 = [item[2] for item in eval_scores] #vmeasure
y4 = [item[3] for item in eval_scores] #purity
y5 = [item[4] for item in eval_scores] #ss
y6 = [item[5] for item in eval_scores] ##dbi


# plotting the measures
fig = plt.plot()
plt.plot(x, y1, label = "ARI")
plt.plot(x, y2, label = "AMI")
plt.plot(x, y3, label = "V-Measure")
#plt.plot(x, y4, label = "Purity")
#plt.plot(x, y5, label = "DB-Index")
#plt.plot(x, y6, label = "Silhouette")


plt.xlabel('Number of clusters')
plt.ylabel('Accuracy Measures')
plt.legend()
#plt.savefig('agg.eps', format='eps', dpi=1200)
#plt.savefig('agg.png', format='png', dpi=1200)
plt.show()


### Input cluster number for fine grained parameter sweep
print('Performing fine-grained parameter sweep ...')
n1 = int(input('Enter lower limit:' ))
n2 = int(input('Enter upper limit:' ))

eval_scores = []
for i in range(n1,n2+1,1):
    temp = agg_clustering(i)
    temp.append(i)
    eval_scores.append(temp)
    
### Tabulate the results
from tabulate import tabulate
print('Results for Agglomerative:')
names = ['ARI', 'AMI', 'V-M', 'Purity', 'Silhouette', 'DB-Index', 'n_cluster']
table = [names]+eval_scores
print(tabulate(table, headers='firstrow', tablefmt='fancy_grid'))
    
