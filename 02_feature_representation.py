### This file is to create feature representations for the different email features.

#Import relevant packages
import pickle
import pandas as pd
import numpy as np
import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"


#Function to process HTML URLs to text and URL
def process_url(url):
    url_value = ''
    url_text = url.text
    for key, value in url.attrs.items():
        if key == 'href':
            url_value = value
    url_dom = urlparse(url_value).netloc
    return url_text, url_value, url_dom
    
#Get a count of dots in a URL
def get_count_dots(url):
    return url.count('.')
    


############# FEATURE 1: EMAIL BODY TEXT
print('EMAIL BODY')
from sentence_transformers import SentenceTransformer

#Read in the dataframe
full_df = pd.read_pickle('datasets/full_dataframe.pkl')
phish_df = pd.read_pickle('datasets/phishing_dataframe.pkl')
enron_df = pd.read_pickle('datasets/enron_dataframe.pkl')

#Encode the email text and extract BERT vectors
bert_model = SentenceTransformer('multi-qa-MiniLM-L6-cos-v1')
bert_vectors = bert_model.encode(full_df['clean_content'])

pickle.dump(bert_vectors, open('outputs/feat_rep_e.pkl', 'wb'))





############# FEATURE 2: SUBJECT LINE
print('SUBJECT LINE')

#Read the subject topic model created in file '00_subject_create_tm.py'
sub_topics = pickle.load(open('outputs/sub_topic_model.pkl', 'rb'))

#Function to check the subject for topic keywords 
def concept_check(sub, topic):
    k = 0
    for item in topic:
        if item in sub:
            k = k+1
        else:
            pass
    return k

#Extract topic vectors for the subject lines
subject_vectors = []
for sub in full_df.clean_subj:
    temp = [concept_check(sub, topic) for topic in sub_topics]
    subject_vectors.append(temp)

pickle.dump(subject_vectors, open('outputs/feat_rep_s.pkl', 'wb'))





############# FEATURE 3: URL FEATURES
print('URL FEATURES')
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from urllib.parse import urlparse

temp_e = pd.read_pickle('outputs/phish_urls_df.pkl')
temp_p = pd.read_pickle('outputs/enron_urls_df.pkl')
url_links = list(temp_p.url_links)+list(temp_e.url_links) #list of url links for the full dataset

url_df = pd.DataFrame()
url_temp_df = pd.DataFrame()

#Extract text, links, domain from the URL list
url_text = []
url_link = []
url_doms = []

for u in url_links:
    temps = []
    if len(u) == 0:
        url_text.append([])
        url_link.append([])
        url_doms.append([])
    elif len(u) == 1:
        temp = process_url(u[0])
        url_text.append([temp[0]])
        url_link.append([temp[1]])
        url_doms.append([temp[2]])
    else:
        url_text.append([process_url(t)[0] for t in u])
        url_link.append([process_url(t)[1] for t in u])
        url_doms.append([process_url(t)[2] for t in u])
        
url_temp_df['text'] = url_text
url_temp_df['link'] = url_link
url_temp_df['domain'] = url_doms

#Number of URLs
url_count = []
for links in url_links:
    url_count.append(len(links))
url_df['url_count'] = url_count

#Length of the URL
url_len = []
for u in url_link:
    temp = 0
    if len(u) == 0:
        #temp.append(0)
        url_len.append(0)
    elif len(u) == 1:
        #temp.append(len(u[0]))
        url_len.append(len(u[0]))
    else:
        for t in u:
            temp = temp + len(t)
        url_len.append(temp/len(u))        
url_df['url_len_avg'] = url_len


#Number of dots in URL
url_dot = []
for i, u in enumerate(url_link):
    #print(i)
    temp = []
    if len(u) == 0:
        temp.append(0)
        url_dot.append(temp[0])
    elif len(u) == 1:
        temp.append(get_count_dots(u[0]))
        url_dot.append(temp[0])
    else:
        for t in u:
            temp.append(get_count_dots(t))
        url_dot.append(max(temp))
url_df['max_url_dot'] = url_dot


#Number of unique domains
n_uni_dom = []
n_empty_dom = []
uni_dom = []
for u in url_doms:
    n_uni_dom.append(len(set([d for d in u if d != ''])))
    n_empty_dom.append(len(u)-len(set([d for d in u if d != ''])))
    uni_dom.append(list(set([d for d in u if d != ''])))
    
url_df['number_uni_domains'] = n_uni_dom
url_df['non_standard_domains'] = n_empty_dom 
url_temp_df['unique_domain'] = uni_dom


#Tranco check for URL domains
tranco_full_list = pickle.load(open("datasets/tranco_list.txt", "rb"))
tranco_list = tranco_full_list[:10000]

url_dom_tranco_check = []
for item in uni_dom:
    k = 0
    for dom in item:
        dom_check = [dom.endswith(item) for item in tranco_list]
        k = k+sum(dom_check)
    url_dom_tranco_check.append(k)
url_df['url_dom_tranco_check'] = url_dom_tranco_check

pickle.dump(url_df, open('outputs/feat_rep_u.pkl', 'wb'))




############# FEATURE 4: HEADER FEATURES
print('HEADER FEATURES')
header_df = pd.DataFrame()
from_list = full_df.from_domain #from-address domains
reply_list = full_df.reply_to #reply_to domains
return_list = full_df.return_path #return_path domains

#From address and URL domain match
def from_url_domain_match(from_dom, url_list):
    k = 0
    for url in url_list:
        if url.endswith(from_dom):
            k = k+1
    return k

from_list = full_df.from_domain #from-address domain
url_dom_list = url_temp_df.unique_domain #unique URL domains

from_url_dom = []
for i in range(full_df.shape[0]):
    from_dom = from_list[i]
    url_list = url_dom_list[i]
    temp = from_url_domain_match(from_dom, url_list)
    from_url_dom.append(temp)
    
header_df['from_url_dom'] = from_url_dom


#Presence of reply-to
temp_reply = [1 for i in reply_list]
for i, dom in enumerate(reply_list):
    if dom == 'None':
        temp_reply[i] = 0
        
#Presence of return-path
temp_return = [1 for i in return_list]
for i, dom in enumerate(return_list):
    if dom == 'None':
        temp_return[i] = 0
        
header_df['reply_to'] = temp_reply
header_df['return_path'] = temp_return

#From-address domain and reply-to/return-path match
import Levenshtein

from_return_check = []
for i in range(len(reply_list)):
    dist = Levenshtein.distance(str(from_list[i]), str(return_list[i]))
    from_return_check.append(dist)
    
from_reply_check = []
for i in range(len(reply_list)):
    dist = Levenshtein.distance(str(from_list[i]), str(reply_list[i]))
    from_reply_check.append(dist)

header_df['from_return'] = from_return_check
header_df['from_reply'] = from_reply_check


#Tranco check with from address domain
from_tranco = []

def dom_tranco_check(dom):
    tranco_full_list = pickle.load(open("datasets/tranco_list.txt", "rb"))
    tranco_list = tranco_full_list[:10000]
    if dom in tranco_list:
        k = 1
    else:
        k = 0
    return k

for em in from_list:
    k = dom_tranco_check(em)
    from_tranco.append(k)
header_df['from_tranco'] = from_tranco


#Save the dataframe for future use
header_df['attachment_count'] = full_df['attachment']

header_df.to_pickle('outputs/feat_rep_h.pkl')
