### The file is to extract URLs from the emails in Phishing and Enron dataset

#Import relevant packages
import mailbox
import pickle
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from urllib.parse import urlparse
import sys
sys.setrecursionlimit(10000)

#Read in the dataframe
enronbox = mailbox.mbox('datasets/emails-enron.mbox') ## path to enron mbox
phishbox = mailbox.mbox('datasets/emails-phishing.mbox') ## path to phishing mbox
full_df = pd.read_pickle('datasets/full_dataframe.pkl')
phish_df = pd.read_pickle('datasets/phishing_dataframe.pkl')
enron_df = pd.read_pickle('datasets/enron_dataframe.pkl')

#Function to extract URLs from email HTML
def flatten(list):
    return [item for sublist in list for item in sublist]
    
def extract_url(msg):
    html = msg.get_payload(decode=True)
    soup = BeautifulSoup(html, features="lxml")
    links = soup.find_all('a')
    return links

def extract_url2(msg):
    html = msg.get_payload()
    soup = BeautifulSoup(str(html), features="lxml")
    links = soup.find_all('a')
    return links


    
#PHISHING DATASET
temp_p = pd.DataFrame()
msg_id = []
url_links = [[] for i in range(len(phishbox))]
err_emails = [] #indices of emails that raise errors
for i, msg in enumerate(phishbox):
    msg_id.append(str(msg['Message-Id']))
    try:
        links = extract_url(msg)
        t = len(links)
        url_links[i] = links
    except:
        err_emails.append(i)   
       

err_temp = []
for i in err_emails:
    temp = []
    msg = phishbox[i]
    html = msg.get_payload()
    for part in html:
        temp.append(extract_url2(part))
    url_links[i] = flatten(temp)
    err_temp.append(flatten(temp))
    
temp_p['id'] = msg_id
temp_p['url_links'] = url_links

t1 = [str(i) for i in full_df[:2193].msg_id]
t2 = [str(i) for i in temp_p.id]
t = list(set(t2).difference(set(t1)))

for i in t:
    temp_p = temp_p.drop(temp_p[temp_p.id == i].index)
    


#ENRON DATASET
temp_e = pd.DataFrame()
msg_id = []
url_links = [[] for i in range(len(enronbox))]
for i, msg in enumerate(enronbox):
    msg_id.append(str(msg['Message-Id']))
    html = msg.get_payload(decode=True)
    soup = BeautifulSoup(html, features="lxml")
    links = soup.find_all('a')
    url_links[i] = links
    
temp_e['id'] = msg_id
temp_e['url_links'] = url_links

t1 = [str(i) for i in full_df[2193:].msg_id]
t2 = [str(i) for i in temp_e.id]
t = list(set(t2).difference(set(t1)))
for i in t:
    temp_e = temp_e.drop(temp_e[temp_e.id == i].index)
    
    
#Save the URL dataframes
#temp_p.to_pickle('outputs/phish_urls_df.pkl')
#temp_e.to_pickle('outputs/enron_urls_df.pkl')
    
    
