# Context-Based Clustering to Mitigate Phishing Attacks

This implementation is part of our paper "Context-based Clustering to Mitigate Phishing Attacks", Tarini Saka, Kami Vaniea, & Nadin Kökciyan, 15th ACM workshop on Artificial Intelligence and Security (AISec 2022) co-located with the 29th ACM Conference on Computer and Communications Security. **[accepted]**

**Abstract:** Phishing is by far the most common and disruptive type of cyberattack faced by most organizations. Phishing messages may share common attributes such as the same or similar subject lines, the same sending infrastructure, similar URLs with certain parts slightly varied, and so on. Attackers use such strategies to evade sophisticated email filters, increasing the difficulty for computing support teams to identify and block all incoming emails during a phishing attack. Limited work has been done on grouping human-reported phishing emails, based on the underlying scam, to help the computing support teams better defend organizations from phishing attacks. In this paper, we explore the feasibility of using unsupervised clustering techniques to group emails into scams that could ideally be addressed together. We use a combination of contextual and semantic features extracted from emails and perform a comparative study on three clustering algorithms with varying feature sets. We use a range of internal and external validation methods
to evaluate the clustering results on real-world email datasets. Our results show that unsupervised clustering is a promising approach
for scam identification and grouping, and analyzing reported phishing emails is an effective way of mitigating phishing attacks and
utilizing the human perspective.

**Code:** We provide the code to perform the various steps in our methodology as described in the paper.


# Requirements
We used Python 3.8.8 to run our experiments. Please check requirements.txt for further dependencies.


# Running the code
