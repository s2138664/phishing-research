### This file is to generate BERT-based topic model for the subject lines

#Import relevant packages
import pickle
import pandas as pd
import numpy as np
import nltk
from scipy.spatial import distance
from nltk.util import ngrams
from sklearn.cluster import KMeans
import collections

#Read in the dataframe
full_df = pd.read_pickle('datasets/full_dataframe.pkl')
phish_df = pd.read_pickle('datasets/phishing_dataframe.pkl')

#Function to flatten a list
def flatten(list):
    return [item for sublist in list for item in sublist]

# Generate tri-grams vocab for the subject-lines
tr_grams_list = [list(ngrams(item.split(), 3)) for item in phish_df.clean_subj]
all_tr_grams = flatten(tr_grams_list)
tr_gram_vocab = [' '.join(gram) for gram in all_tr_grams]

# Encode the trigram vocabulary to create BERT vectors
from sentence_transformers import SentenceTransformer
bert_model = SentenceTransformer('multi-qa-MiniLM-L6-cos-v1')
bert_vect_tr = bert_model.encode(tr_gram_vocab)

# Get average embeddings for all the tri-grams
df_t = pd.DataFrame()
df_t['words'] = tr_gram_vocab
df_t['embeddings'] = list(bert_vect_tr)
tri_dict = {}
for tri in set(tr_gram_vocab):
    temp = df_t[df_t.words == tri]
    mean_tri_emb = np.mean(temp.embeddings)
    tri_dict[tri] = mean_tri_emb


# Cluster the BERT vectors to get topic clusters
n = 30 #number of topics to generate
kmeans_model_tr = KMeans(n_clusters = n, init='k-means++',n_init = 20, max_iter=100, random_state=0).fit(list(tri_dict.values()))

labels_tri = kmeans_model_tr.labels_
centers_tri = kmeans_model_tr.cluster_centers_
df = pd.DataFrame()
df['words'] = list(tri_dict.keys())
df['embeddings'] = list(tri_dict.values())
df['labels'] = labels_tri

#Extract top 20 keywords from each clusters based on frequency
topic_keywords = []
for i in set(labels_tri):
    import collections
    temp = df[df.labels == i]
    topic = list(temp.words)
    topic_words = ' '.join(topic).split()
    topic_dict = collections.Counter(topic_words)
    keys = [key[0] for key in topic_dict.most_common(20)]
    topic_keywords.append(keys)
    
#Save the topic model    
#pickle.dump(topic_keywords, open('outputs/sub_topic_model.pkl', 'wb'))




