### This file read in the phishing and enron mboxes, preprocesses the emails, extracts all the relevant information and stores them in a dataframe

#Import necessary packages
import mailbox
import pandas as pd
from bs4 import BeautifulSoup
import random
import string
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords') # Download stop words
stopwords_english = stopwords.words('english')
import warnings
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')

#Extract content simple 'text/' type emails/parts
def text_content(msg):
    html = msg.get_payload(decode=True)
    soup = BeautifulSoup(html,features="lxml")
    return soup.get_text()

#Extract content from 'multipart/alternative' type
def ma_content(msg):
    for part in msg.get_payload():
        html = part.get_payload(decode=True)
        soup = BeautifulSoup(html, features="lxml")
    return soup.get_text()

#Extract content from 'multipart/related' type
def mr_content(msg):
    temp = []
    for part in msg.get_payload():
        if part.get_content_type() == 'text/plain' or part.get_content_type() ==  'text/html':
            temp.append(text_content(part))
        elif part.get_content_type() == 'multipart/alternative':
            temp.append(ma_content(part))
    content = temp
    return content

#Extract content from 'multipart/mixed' type
def mm_content(msg):
    temp = []
    for part in msg.get_payload():
        if part.get_content_type().startswith("text/"):
            temp.append(text_content(part))
        elif part.get_content_type() == 'multipart/alternative':
            temp.append(ma_content(part))
        elif part.get_content_type() == 'multipart/related':
            temp.append(mr_content(part))
    return temp

#Function to extract content from multipart emails
def process_content(msg):
    if msg.get_content_type().startswith("text/"):
        cont = text_content(msg)
    elif msg.get_content_type() == 'multipart/alternative':
        cont = ma_content(msg)
    elif msg.get_content_type() == 'multipart/related':
        cont = mr_content(msg)
    elif msg.get_content_type() == 'multipart/mixed':
        cont = mm_content(msg)
    return cont

#Feed the extracted content from the email body
def extract_words(content):
    content = content.translate(str.maketrans(string.punctuation, ' '*len(string.punctuation)))
    content = content.replace( "\t" , " ").replace("\n" , " ")
    content = content.lower()
    words = content.split()
    words = [i for i in words if len(i) > 1]
    temp = list() # Remove any stopwords in the list
    for word in words:
        if (word not in stopwords_english and
            word not in string.punctuation):
            temp.append(word)
    words = temp
    return(words)
    
#Function to obtain domain from an email address
def extract_domain(em):
    if em:
        temp = em.split('@')[-1].replace(">", "")
    else:
        temp = 'None'
    return temp

#Function to extract the attachment count in emails
def extract_attachment_count(msg):
    #extract the content-disposition for all parts
    if msg.is_multipart():
        temp = [part['Content-Disposition'] for part in msg.get_payload()]
    else:
        temp = [msg['Content-Disposition']]
    #extract the count from temp list
    k = 0
    for t in temp:
        if str(t).startswith("attachment"):
            k = k+1
        else:
            pass
    return k
    

### Read the Dataset and initialize the Dataframe
en_mbox = mailbox.mbox('datasets/emails-enron.mbox') ## path to enron mbox
ph_mbox = mailbox.mbox('datasets/emails-phishing.mbox') ## path to phishing mbox
phish_df = pd.DataFrame()
enron_df = pd.DataFrame()




### ENRON DATAFRAME
print('Start - Enron')
# Initial email features
from_list = []
date_list = []
to_list = []
subject_list = []
multipart_list = []
content_list = []
msg_id = []
from_add = []
reply_list = []
return_list = []

# Adding the values to the DataFrame
for i, message in enumerate(en_mbox):
    msg_id.append(message['Message-ID'])
    from_list.append(message['from'])
    date_list.append(message['date'])
    to_list.append(message['to'])
    subject_list.append(message['subject'])
    multipart_list.append(message.is_multipart())

for i, msg in enumerate(en_mbox):
    from_add.append(msg['From'].split('@')[-1])
    return_list.append(msg['Return-Path'])
    if msg['Reply-To']:
        reply_list.append(msg['Reply-To'])
    else:
        reply_list.append('None')
        
enron_df['msg_id'] = msg_id
enron_df['From'] = from_list
enron_df['To'] = to_list
enron_df['Date'] = date_list
enron_df['Subject'] = subject_list
enron_df['Multipart'] = multipart_list
enron_df['from_domain'] = from_add
enron_df['return_path'] = return_list
enron_df['reply_to'] = reply_list
enron_df['attachment'] = [0 for i in msg_id]

# Process content of all the emails
words_list = []
content_list = []
temp_list = []

for i, msg in enumerate(en_mbox):
    words = []
    if msg.is_multipart():
        content = process_content(msg)
        if isinstance(content, list):
            for item in content:
                if isinstance(item, list):
                    for p in item:
                        words.append(extract_words(p))
                elif isinstance(item, str):
                    words.append(extract_words(item))
        elif isinstance(content, str):
            words.append(extract_words(content))

    else:
        content = text_content(msg)
        words.append(extract_words(content))

    content_list.append(content)
    words_list.append(words)

for l in words_list:
    temp_list.append(l[0])

# Add content and word to the dataframe
enron_df['Content'] = content_list
enron_df['Words'] = temp_list

# Remove punctuation and stopwords from subject
def clean_text(text):
    text = str(text).lower()
    text = text.translate(str.maketrans(string.punctuation, ' '*len(string.punctuation)))
    cleaned = [word for word in text.split() if not word in stopwords_english]
    return ' '.join(cleaned)
# Add cleaned subject line to the dataframe
temp = [clean_text(sub) for sub in enron_df.Subject]
enron_df['clean_subj'] = temp

# Extract domains from the from-address
from_list = []
for i, em in enumerate(enron_df.From):
    email = str(em).split('<')[-1].replace(">", "")
    from_list.append(email)
enron_df['from_domain'] = from_list



# Add the 'benign' tag to this dataset
tag = ['Benign' for i in enron_df['Words'] ]
enron_df['Tag'] = tag

# Take a subset to match phishing email length
enron_sub_df = enron_df.sample(n=2193, random_state=1, ignore_index = True)

# Save the dataframe for future use
#enron_sub_df.to_pickle('datasets/enron_dataframe.pkl')
print('End - Enron')




### PHISHING DATAFRAME
print('Start - Phish')
# Initialize email features
from_list = []
date_list = []
to_list = []
subject_list = []
multipart_list = []
msg_id = []
reply_list = []
return_list = []
from_add = []

for i, message in enumerate(ph_mbox):
    msg_id.append(str(message['Message-ID']))
    from_list.append(str(message['from']))
    date_list.append(message['date'])
    to_list.append(str(message['to']))
    subject_list.append(str(message['subject']))
    multipart_list.append(message.is_multipart())


for i, msg in enumerate(ph_mbox):
    em = str(msg['From'])
    try: 
        temp2 = em.split('<')[1].replace(">", "")
        try:
            temp = temp2.split('@')[1]
        except:
            temp = temp2
    except:
        temp = em.split('@')[1]
    from_add.append(temp)


for i, msg in enumerate(ph_mbox):
    em = str(msg['Return-Path'])
    try: 
        temp = em.split('<')[1].replace(">", "").split('@')[1]
    except:
        temp = em.split('@')[1]
    return_list.append(temp)


for i, msg in enumerate(ph_mbox):
    if msg['Reply-To']:
        em = str(msg['Reply-To'])
        try:
            temp2 = em.split('<')[1].replace(">", "")
            try:
                temp = temp2.split('@')[1]
            except:
                temp = temp2
        except:
            temp = em.split('@')[1]
        reply_list.append(temp)
    else:
        reply_list.append('None')



phish_df['msg_id'] = msg_id
phish_df['From'] = from_list
phish_df['To'] = to_list
phish_df['Date'] = date_list
phish_df['Subject'] = subject_list
phish_df['Multipart'] = multipart_list
phish_df['from_domain'] = from_add
phish_df['return_path'] = return_list
phish_df['reply_to'] = reply_list
phish_df['attachment'] = [extract_attachment_count(msg) for msg in ph_mbox]

# Process content of all the emails
content_list = []
for i, msg in enumerate(ph_mbox):
    if msg.is_multipart():
        content = process_content(msg)
    else:
        content = text_content(msg)
    content_list.append(content)

# Flattening the content column
temp = []
for i, c in enumerate(content_list):
    if isinstance(c, list):
        try:
            temp.append(' '.join(c))
        except:
            temp.append(' '.join(c[0]))
    else:
        temp.append(c)

# Extract words from the content
words_list = []
for content in temp:
    words_list.append(extract_words(content))

# Append the list of contents to the DataFrame
phish_df['Content'] = temp
phish_df['Words'] = words_list


# Remove punctuation and stopwords from subject
def clean_text(text):
    text = str(text).lower()
    text = text.translate(str.maketrans(string.punctuation, ' '*len(string.punctuation)))
    cleaned = [word for word in text.split() if not word in stopwords_english]
    return ' '.join(cleaned)
# Add cleaned subject line to the dataframe
temp = [clean_text(sub) for sub in phish_df.Subject]
phish_df['clean_subj'] = temp

# Extract the domains from the from-address
from_list = []
for i, em in enumerate(phish_df.From):
    email = str(em).split('<')[-1].replace(">", "")
    from_list.append(email)
phish_df['from_domain'] = from_list

# Add the 'phish' tag to this dataset
tag = ['Phish' for i in phish_df['Words'] ]
phish_df['Tag'] = tag


# Remove rows with empty content/words
indices = [i for i, x in enumerate(phish_df['Words'] ) if len(x) == 0]
indices1 = [i for i, x in enumerate(phish_df['Content'] ) if x == '']
indices2 = [i for i, x in enumerate(phish_df['Content'] ) if x == ' ']
indices4 = [i for i, x in enumerate(phish_df['Words'] ) if x == [[]]]
ind = indices + indices1 + indices2 + indices4
phish_df.drop(phish_df.index[ind], inplace=True)
# Reset the index
phish_df.index = [i for i in range(phish_df.shape[0])]

# Save the dataframe for future use
#phish_df.to_pickle('datasets/phishing_dataframe.pkl')
print('End - Phish')

### Join the dataframes
full_df  = pd.concat([phish_df, enron_sub_df], ignore_index=True, axis = 0)
# Pre-process the email body text 
temp = [' '.join(full_df.Words[i]) for i in range(full_df.shape[0])]
full_df['clean_content'] = temp
# Save the full dataframe for future use
#full_df.to_pickle('datasets/full_dataframe.pkl')
